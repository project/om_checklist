<?php
// $Id$

/**
 * @file
 * Admin page callbacks for the open media module.
 */

/**
 * Menu callback; prints a page listing steps to configure open media.
 */
function om_checklist_main() {
  // Add CSS
  //drupal_add_css(drupal_get_path('module', 'openmedia') . '/openmedia.css');
  $output = '<h2>' . t('Status of Open Media configuration:') . '</h2>' . om_checklist_steps_as_list();
  return $output;
}

/**
 * Menu callback; prints a page listing general help for a module.
 */
function om_checklist_page($name) {
  $output = 'steps...';
  return $output;
}

function om_checklist_steps_as_list() {
  
  //defines constants for REQUIREMENT_INFO, REQUIREMENT_OK, REQUIREMENT_WARNING, REQUIREMENT_ERROR
  include_once DRUPAL_ROOT . '/includes/install.inc';
  
  define('REQUIREMENT_NOT_INSTALLED', -1);
  
  //Format status report using theme_status_report
  //$requirements[0] = array('title' => 'Drupal', 'value' => '7.0-dev', 'severity' => 2, 'weight' => -10, 'description' => 'You must upgrade PHP to 5.2.8 or greater.');
    
  //PHP
  $version = phpversion();
  $description = t('You are using version @version of PHP.', array('@version' => $version));
  $requirements['php'] = array(
    'title' => 'PHP', 
    'value' => $version, 
    'weight' => -10, 
  );
     
  if (version_compare(phpversion(), '5.2') < 0) {
   $requirements['php']['severity'] = 2; 
   $requirements['php']['description'] = $description . ' You must upgrade to PHP 5.3 or greater for several core Open Media functions to work correctly.';
  }
  else {
    $requirements['php']['severity'] = 0; 
    $requirements['php']['description'] = $description;   
  }
  
  //RAM
  //max_execution_time
  //memory_limit
  //ini_get();

  $memory_limit = ini_get('memory_limit');
  $description = t('You have @memory_limit of memory allocated PHP.', array('@memory_limit' => $memory_limit));
  $requirements['memory'] = array(
    'title' => 'Memory', 
    'value' => $memory_limit, 
    'weight' => -10, 
  );
  
  $memory_needs = 64;
  
  if (module_exists('merci')) {
    $memory_needs = ($memory_needs + 32);
  }
  
  if (module_exists('civicrm')) {
    $memory_needs = ($memory_needs + 32);
  }
  
  $memory_limit_int = str_replace('M', '', $memory_limit);
   
  if ($memory_limit_int > ($memory_needs + 100)) {
   $requirements['memory']['severity'] = REQUIREMENT_OK; 
   $requirements['memory']['description'] = $description . ' You have more than enough memory allocated to PHP to run the enabled modules.';
  }
  
  if ($memory_limit_int > $memory_needs) {
   $requirements['memory']['severity'] = REQUIREMENT_OK; 
   $requirements['memory']['description'] = $description . ' You have enough memory allocated to PHP to run the enabled modules.';
  }
  
  if ($memory_limit_int = $memory_needs) {
   $requirements['memory']['severity'] = REQUIREMENT_WARNING; 
   $requirements['memory']['description'] = $description . ' You have just enough memory allocated to PHP to run the enabled modules.  If you experience performance issues, consider allocating more memory to PHP.';
  }
  
  if ($memory_limit_int < $memory_needs) {
    $requirements['memory']['severity'] = REQUIREMENT_ERROR; 
    $requirements['memory']['description'] = $description . ' You will likely have performance problems unless you allocate at least ' . $memory_needs . ' of RAM to PHP.';   
  }
  
  //Airing
  $requirements['om_airing'] = array(
    'title' => 'Airing', 
    'weight' => -8, 
  );
  
  if (module_exists('om_airing')) {
    $om_airing_status = om_airing_status();
    $requirements['om_airing']['value'] = 'Installed';
    $requirements['om_airing']['severity'] = $om_airing_status['severity'];
    $requirements['om_airing']['description'] = $om_airing_status['description'];
  } 
  else {
    $requirements['om_airing']['value'] = 'Not Installed';
    $requirements['om_airing']['severity'] = REQUIREMENT_NOT_INSTALLED; 
    $requirements['om_airing']['description'] = t('The ') . l('Open Media Airing', 'http://drupal.org/project/om_airing') . t(' module is not installed.') . t(' Use this module to display scheduling information from playback server RSS. It includes Calendar based Views and Blocks that display upcoming airings on a Show node.'); 
    $requirements['om_airing']['description'] .= '<p>' . t('Installation Difficulty') .': <b>' . t('Low') . '</b></p>';
  }
  
    
  //Crew Connect
  $requirements['om_crew_connect'] = array(
    'title' => 'Crew Connect', 
    'weight' => -7, 
  );
  
  if (module_exists('om_crew_connect')) {
    $om_crew_connect_status = om_crew_connect_status();
    $requirements['om_crew_connect']['value'] = 'Installed';
    $requirements['om_crew_connect']['severity'] = $om_crew_connect_status['severity'];
    $requirements['om_crew_connect']['description'] = $om_crew_connect_status['description'];
  } 
  else {
    $requirements['om_crew_connect']['value'] = 'Not Installed';
    $requirements['om_crew_connect']['severity'] = REQUIREMENT_NOT_INSTALLED; 
    $requirements['om_crew_connect']['description'] = t('The ') . l('Crew Connect', 'http://drupal.org/project/om_crew_connect') . t(' module is not installed.') . t(' Use this module help producers connect with each other.  Enabling Crew Connect gives your producers a reason to login to your site and gives you experience managing users and permissions.'); 
    $requirements['om_crew_connect']['description'] .= '<p>' . t('Installation Difficulty') .': <b>' . t('Low') . '</b></p>';
  }
  
   //CiviCRM
  $requirements['civicrm'] = array(
    'title' => 'CiviCRM', 
    'weight' => -6, 
  );
  
  if (module_exists('civicrm')) {
    $om_checklist_civicrm_status = om_checklist_civicrm_status();
    $requirements['civicrm']['value'] = 'Installed.';
    $requirements['civicrm']['severity'] = $om_checklist_civicrm_status['severity'];
    $requirements['civicrm']['description'] = $om_checklist_civicrm_status['description'];
  } 
  else {
    $requirements['civicrm']['value'] = 'Not Installed';
    $requirements['civicrm']['severity'] = REQUIREMENT_NOT_INSTALLED; 
    $requirements['civicrm']['description'] =  l('CiviCRM', 'http://drupal.org/project/civicrm') . t(' '); 
    $requirements['om_crew_connect']['description'] .= '<p>' . t('Installation Difficulty') .': <b>' . t('Medium') . '</b></p>';
  }

  
  //PBCore
  $requirements['pbcore'] = array(
    'title' => 'PBCore', 
    'weight' => -7, 
  );
  
  if (module_exists('pbcore')) {
    $pbcore_status = pbcore_status();
    $requirements['pbcore']['value'] = 'Installed';
    $requirements['pbcore']['severity'] = $pbcore_status['severity'];
    $requirements['pbcore']['description'] = $pbcore_status['description'];
  } 
  else {
    $requirements['pbcore']['value'] = 'Not Installed';
    $requirements['pbcore']['severity'] = REQUIREMENT_NOT_INSTALLED; 
    $requirements['pbcore']['description'] = t('The ') . l('PBCore', 'http://drupal.org/project/om_crew_connect') . t(' module enables producers to categorize their shows based on this metadata dictionary. It is required for Project and Show.'); 
    $requirements['pbcore']['description'] .= '<p>' . t('Installation Difficulty') .': <b>' . t('Low') . '</b></p>';
  }
  
  //Creative Commons
  $requirements['creativecommons'] = array(
    'title' => 'Creative Commons', 
    'weight' => -7, 
  );
  
  if (module_exists('creativecommons')) {
    $creativecommons_status = creativecommons_status();
    $requirements['creativecommons']['value'] = 'Installed';
    $requirements['creativecommons']['severity'] = $creativecommons_status['severity'];
    $requirements['creativecommons']['description'] = $creativecommons_status['description'];
  } 
  else {
    $requirements['creativecommons']['value'] = 'Not Installed';
    $requirements['creativecommons']['severity'] = REQUIREMENT_NOT_INSTALLED; 
    $requirements['creativecommons']['description'] = l('Creative Commons', 'http://drupal.org/project/om_crew_connect') . t(' adds licensing options to Show.  Managing rights is complicated and you should understand the difference is the Creative Commons license before enabling this module.'); 
    $requirements['creativecommons']['description'] .= '<p>' . t('Installation Difficulty') .': <b>' . t('Low') . '</b></p>';
  }
  
  //Project
  $requirements['om_project'] = array(
    'title' => 'Project', 
    'weight' => -8, 
  );

  if (module_exists('om_project')) {
    $om_project_status = om_project_status();
    $requirements['om_project']['value'] = 'Installed';
    $requirements['om_project']['severity'] = $om_project_status['severity']; 
    $requirements['om_project']['description'] = $om_project_status['description'];
  } 
  else {
    $requirements['om_project']['value'] = 'Not Installed';
    $requirements['om_project']['severity'] = REQUIREMENT_NOT_INSTALLED; 
    $requirements['om_project']['description'] = t('The ') . l('Open Media Project', 'http://drupal.org/project/om_project') . t(' module is not installed.') . t(' Use this module to group Shows and Reservations.  The module leverages Organic Groups (OG).  There are dozens of popular OG modules that extend social networking features.  This module includes Views and Blocks that can be used to create attractive Project pages.'); 
    $requirements['om_project']['description'] .= '<p>' . t('Installation Difficulty') .': <b>' . t('Low') . '</b></p>';
  }
  
  //MERCI
  $requirements['merci'] = array(
    'title' => 'MERCI', 
    'weight' => -8, 
  );
  if (module_exists('merci')) {
    $om_checklist_merci_status = om_checklist_merci_status();
    $requirements['merci']['value'] = 'Installed.';
    $requirements['merci']['severity'] = $om_checklist_merci_status['severity'];
    $requirements['merci']['description'] = $om_checklist_merci_status['description'];
  } 
  else {
    $requirements['merci']['value'] = 'Not Installed';
    $requirements['merci']['severity'] = REQUIREMENT_NOT_INSTALLED; 
    $requirements['merci']['description'] = t('The ') . l('MERCI', 'http://drupal.org/project/merci') . t(' module is not installed.') . t(' Used to Manage Equipment Reservations, Checkout, and Inventory.'); 
    $requirements['merci']['description'] .= '<p>' . t('Installation Difficulty') .': <b>' . t('Moderate') . '</b></p>';
   
  }
 
  
  //Show
  $requirements['om_show'] = array(
    'title' => 'Show', 
    'weight' => -8, 
  );
   
  if (module_exists('om_show')) {
    $om_show_status = om_show_status();
    $requirements['om_show']['value'] = 'Installed';
    $requirements['om_show']['severity'] = $om_show_status['severity']; 
    $requirements['om_show']['description'] = $om_show_status['description'];
  } 
  else {
    $requirements['om_show']['value'] = 'Not Installed';
    $requirements['om_show']['severity'] = REQUIREMENT_NOT_INSTALLED; 
    $requirements['om_show']['description'] = t('The ') . l('Open Media Show', 'http://drupal.org/project/om_show') . t(' module is not installed.') . t(' Show manages the core metadata required to exchange video with other sites.  Show can be used with an existing encoding workflows or with a workflow manage by other Drupal modules.'); 
    $requirements['om_show']['description'] .= '<p>' . t('Installation Difficulty') .': <b>' . t('Moderate') . '</b></p>';
   
  }
  
  //Timeslot Scheduler
  $requirements['om_timeslot_scheduler'] = array(
    'title' => 'Timeslot Scheduler', 
    'weight' => -8, 
  );
  
  if (module_exists('om_timeslot_scheduler')) {
    $om_timeslot_status = om_timeslot_status();
    $requirements['om_timeslot_scheduler']['value'] = 'Installed';
    $requirements['om_timeslot_scheduler']['severity'] = $om_timeslot_status['severity']; 
    $requirements['om_timeslot_scheduler']['description'] = $om_timeslot_status['description'];
  } 
  else {
    $requirements['om_timeslot_scheduler']['value'] = 'Not Installed';
    $requirements['om_timeslot_scheduler']['severity'] = REQUIREMENT_NOT_INSTALLED; 
    $requirements['om_timeslot_scheduler']['description'] = t('The ') . l('Open Media Timeslot Scheduler', 'http://drupal.org/project/om_timeslot_scheduler') . t(' module is not installed.') . t(' Used to automate scheduling and/or enable producers to self schedule.'); 
    $requirements['om_timeslot_scheduler']['description'] .= '<p>' . t('Installation Difficulty') .': <b>' . t('Advanced') . '</b></p>';
   
  }
    
  return theme('status_report', array('requirements' => $requirements));
}


